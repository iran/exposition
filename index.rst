
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️
.. 🇮🇷


.. _exposition_iran:
.. _iran_exposition:

===========================================================================================================================
🇮🇷  **Exposition Femme Vie Liberté** du lundi 7 mars au vendredi 31 mars 2023 à la Maison de l’International à Grenoble
===========================================================================================================================


.. figure:: images/logo_exposition.png
   :align: center
   :width: 300


L'exposition **Femme Vie Liberté** se donne comme objectif de mettre en
lumière la lutte des femmes iraniennes.
Elle décrira la répression en Iran depuis la révolution de 1979 et
tentera de retracer la révolution en cours depuis le meurtre de Mahsa
Jina Amini par le gouvernement iranien.

L'exposition comprend également une partie artistique en relation avec
**Femme Vie Liberté**, exposant des œuvres de peinture et de céramique
produites par des artistes iraniennes grenobloises.


.. figure:: images/mahsa_amini.png
   :align: center


.. toctree::
   :maxdepth: 6

   grenoble/grenoble
   ressources/ressources
