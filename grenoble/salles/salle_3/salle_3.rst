
.. index::
   pair: Salle 3 (histoire de la révolution 2022-2023 en Iran); Grenoble

.. _salle_3_grenoble:

=========================================================================================================
**Salle 3 de l'exposition, histoire de la révolution 2022-2023 en Iran**
=========================================================================================================


Entrée salle 3
================

.. figure:: images/entree_salle_3.png
   :align: center

.. figure:: images/8_mars_1979.jpeg
   :align: center


Révolution Mahsa Jina Amini
===============================

.. figure:: images/revolution_mahsa_jina_amini.jpeg
   :align: center

.. figure:: images/jina_mahsa_amini.jpeg
   :align: center

.. figure:: images/revolution_mahsa_jina_amini_2.jpeg
   :align: center


Vue d'ensemble
===============================

.. figure:: images/vue_densemble.jpeg
   :align: center


2 faces
===============================

.. figure:: images/2_faces.jpeg
   :align: center


4 faces
===============================

.. figure:: images/4_faces.jpeg
   :align: center


Manifestations dans le monde et à Grenoble
==============================================

.. figure:: images/manifestations.jpeg
   :align: center


Manifestations dans le monde
===============================

.. figure:: images/manifestations_2.jpeg
   :align: center

Manifestations dans le monde et à Grenoble
===============================================

.. figure:: images/manifestations_3.jpeg
   :align: center

Manifestations dans le monde et à Grenoble
===============================================

.. figure:: images/manifestation_grenoble_she.jpeg
   :align: center


**Liberté, Down with Islamic Republic of Iran,  Down with forced hijab**
=============================================================================

.. figure:: images/down_with_forced_hijab.jpeg
   :align: center


**Meurtre des jeunes**
=============================================================================

.. figure:: images/meurtres_des_jeunes.jpeg
   :align: center



Shervin Hajipour arrêté le 29 septembre 2022
=================================================

- :ref:`iran_luttes:shervin_hajipour`

.. figure:: images/shervin_hajipour_2022_09_29.jpeg
   :align: center

   :ref:`iran_luttes:shervin_hajipour`


Evin 15 octobre 2022
===============================

.. figure:: images/evin_2022_10_15.jpeg
   :align: center


40e jour de la mort de Jina Mahsa Amini (26 octobre 2022)
============================================================

.. figure:: images/40e_jour_mort_jina_mahsa_amini.jpeg
   :align: center


Manifestations à Grenoble le 17 décembre 2022
==================================================

- :ref:`iran_luttes:iran_grenoble_2022_12_17`

.. figure:: images/manifestation_grenoble.jpeg
   :align: center

   :ref:`iran_luttes:iran_grenoble_2022_12_17`


|BaharehAkrami| Dessins de Bahareh Akrami
========================================================================

- :ref:`akrami_iran:dessins_bahareh_akrami`


Semaine 52 de 2022
----------------------

:ref:`akrami_iran:bahareh_akrami_2022_52`

.. figure:: images/bahareh_akrami_semaine_52_2022.jpeg
   :align: center

   |BaharehAkrami| :ref:`akrami_iran:bahareh_akrami_2022_52`


Semaine 50 de 2022
----------------------

:ref:`akrami_iran:bahareh_akrami_2022_50`

.. figure:: images/bahareh_akrami_semaine_50_2022.jpeg
   :align: center

   |BaharehAkrami| :ref:`akrami_iran:bahareh_akrami_2022_50`

Semaine 47 de 2022
----------------------

:ref:`akrami_iran:bahareh_akrami_2022_47`

.. figure:: images/bahareh_akrami_semaine_47_2022.jpeg
   :align: center

   |BaharehAkrami| :ref:`akrami_iran:bahareh_akrami_2022_47`

Semaine 46 de 2022
----------------------

:ref:`akrami_iran:bahareh_akrami_2022_46`

.. figure:: images/bahareh_akrami_semaine_46_2022.jpeg
   :align: center

   |BaharehAkrami| :ref:`akrami_iran:bahareh_akrami_2022_46`



|KianPirfalak| Assassinat de Kian Pirfalak 9 ans le 15 novembre 2022
========================================================================

- :ref:`iran_luttes:kian_pirfalak`
- :ref:`akrami_iran:kian_2022_11_16`

.. figure:: images/kian_pirfalak.jpeg
   :align: center


Grenoble **Veillée de soutien aux victimes de la répression en Iran** organisée par Amnesty International Grenoble
=====================================================================================================================

-:ref:`iran_luttes:veillee_2023_02_10`

.. figure:: images/veille_grenoble_2023_02_10.jpeg
   :align: center

No war
========================================================================

.. figure:: images/no_war.jpeg
   :align: center




Forces de repression en Iran
================================================================

.. figure:: images/forces_de_repression_en_iran.jpeg
   :align: center


Economie de l'Iran
================================================================

.. figure:: images/economie_iran.jpeg
   :align: center



Points forts, faiblesses et avenir du mouvement
=================================================

.. figure:: images/points_forts_faiblesses_et_avenir_du_mouvement.jpeg
   :align: center


TODO actuel 1
=================

.. figure:: images/actuel_1.png
   :align: center


TODO actuel 2
=================

.. figure:: images/actuel_2.png
   :align: center


TODO actuel 3
=================

.. figure:: images/actuel_3.png
   :align: center

