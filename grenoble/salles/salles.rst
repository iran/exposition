
.. index::
   pair: Salles ; Grenoble
   ! Salles

.. _salles_grenoble:

=========================================================================================================
**Les 4 salles de l'exposition**
=========================================================================================================


.. toctree::
   :maxdepth: 3

   salle_1/salle_1
   salle_2/salle_2
   salle_3/salle_3
   salle_4/salle_4

