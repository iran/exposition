
.. index::
   pair: Salle 2 ; Grenoble

.. _salle_2_grenoble:

============================================================================================================
**Salle 2 de l'exposition, salle des concerts et histoire de la répression depuis la révolution de 1979**
============================================================================================================



Mur 1
======


.. figure:: images/mur_1.jpeg
   :align: center


Mur 2
======


.. figure:: images/mur_2.jpeg
   :align: center


Premières exécutions 1979
=================================

.. figure:: images/premieres_execution_1979.jpeg
   :align: center

Comité de la mort Raisi 1988
=================================

.. figure:: images/comite_de_la_mort_raisi_1988.jpeg
   :align: center


Terreur 1990-1998
====================

.. figure:: images/terreur_1990_1998.jpeg
   :align: center


Confrontation 1999
=====================

.. figure:: images/confrontation_1999.jpeg
   :align: center


Mouvement vert 2009
=====================

.. figure:: images/mouvement_vert_2009.jpeg
   :align: center


Fin des espoirs années 2010
================================

.. figure:: images/fin_des_espoirs_annees_2010.jpeg
   :align: center


|ps752justice| Le 8 janvier 2020, le Boeing 737 NG effectuant le vol 752 d'Ukraine International Airlines (PS 752) est abattu par la défense anti-aérienne iranienne
=========================================================================================================================================================================

- https://fr.wikipedia.org/wiki/Vol_Ukraine_International_Airlines_752

Le 8 janvier 2020, le Boeing 737 NG effectuant le vol 752 d'Ukraine
International Airlines (PS 752), devant relier Téhéran, capitale de l'Iran,
à Kiev (Ukraine), est abattu par erreur par la défense anti-aérienne
iranienne peu après son décollage de Téhéran.

Les 176 personnes à bord, dont 138 se rendaient au Canada, périssent
dans l'accident.
C'est le premier accident mortel de l'histoire de la compagnie Ukraine
International Airlines et le plus meurtrier survenu en Iran depuis
l'accident d'un Iliouchine Il-76 en 2003 qui avait provoqué la mort
de 275 Gardiens de la révolution.

C'est également le plus grave pour le Canada depuis celui du vol 182
d'Air India en 1985, qui a coûté la vie à 268 Canadiens.

C'est le pire accident impliquant un Boeing 737 NG, surpassant celui
du vol 812 Air India Express en 2010, et le deuxième le plus meurtrier
d'un Boeing 737 après celui du 737 Max 8 de dernière génération du
vol 610 de Lion Air en 2018.

Le crash a eu lieu alors que la défense aérienne iranienne était en
état d'alerte en raison d'une grande tension entre l'Iran et les
États-Unis.

Dès le lendemain de l'accident, le Premier ministre canadien Justin
Trudeau déclare que le Boeing aurait été abattu par un missile sol-air
Tor-M15, se basant sur les informations des services de renseignement
alliés, ce qui est initialement réfuté par les autorités iraniennes.

Le 11 janvier, celles-ci admettent avoir abattu l'avion par erreur et
précisent le 21 janvier qu'il y eut deux tirs de missiles.

La révélation de la tentative de dissimulation de la responsabilité des
forces armées par le gouvernement entraine une reprise des manifestations
antigouvernementales dans le pays.



.. figure:: images/ps752_2020.jpeg
   :align: center




Concert du 7 mars 2023
======================


.. figure:: images/concert_2023_03_07.png
   :align: center



Vidéos
==========

.. youtube:: nxt8PiL4z9Q

برای توی کوچه رقصیدن

::

    Barâyé touyé koutché raghsidan
    Pour danser dans la rue

برای ترسیدن به وقت  بوسیدن

::

    Barâýé tarsidan bé vaghté boussidan
    Pour la peur lors de s’embrasser

برای خواهرم خواهرت خو اهرامون

::

    Barâýé khaharam khaharet khaharamoun
    Pour ma sœur, ta sœur, nos sœurs

برای تغییر مغز ها که پوسیدن

::

    Barâýé taaghiiré maghzha keh poussidan
    Pour changer une mentalité pourrie

برای شرمندگی برای بی پولی

::

    Barâýé chamandégui Barâýé bi pouli
    Pour la honte du manque d’argent

برای حسرت یک زندگی معمولی

::

    Barâýé hsraté yek zéndéguié maaouli
    Pour le regret de ne pas avoir une vie normale

برای کودک زباله گرد و آرزوهاش

::

    Barâýé koudaké zobalégard o ârézougach
    Pour l’enfant qui cherche dans les ordures et pour ses rêves

برای این اقتصاد دستوری

::

    Barâýé în éghtéssadé destouri
    Pour cette économie imposée.

برای این هوای آلوده

::

    Barâýé in havayé âloudé
    Pour cet air pollué

برای ولی عصر و درختای فرسوده

::

    Barâýé valuasr o dérakhtâyé farssoudeh
    Pour l’avenue Vali-Asr et ses arbres sénescents

برای پیروز و احتمال انقراضش

::

    Barâýé pirouz oehtémalé énghérazech
    Pour le guépard en voie probable d’extinction

برای سگهای بی گناه ممنوعه

::

    Barâýé sag hayé bi gonahé mamnouéé
    Pour les chiens innocents interdits


برای گریه های بی وقفه

::

    Barâýé gueryé hayé bi vaghfeh
    Pour les pleurs ininterrompus

برای تکرار تصویر این لحظه

::

    Barâýé tekrâré tasviré in lahzeh
    Pour l’image répétée de cet instant

برای چهره ای که می خنده

::

    Barâýé tchehreh ii keh mikhandeh
    Pour un visage qui sourit


برای دانش آموزان برای آینده

::

    Barâýé danesamouzan barâýé âyandeh
    Pour les élèves, pour l’avenir

برای این بهشت اجباری

::

    Barâýé in behechté éjbari
    Pour ce paradis obligatoire

برای نخبه های زندان ی

::

    Barâýé nokhbeh hayé zéndàni
    Pour les étudiants brillants emprisonnés

برای کودکان افغانی

::

    Barâýé koudakâné afghani
    Pour les enfants afghans

برای این همه برای غیر  تکراری

::

    Barâýé in hameh Barâ haýé gheiré tekrâri
    Pour tous ces « pour » à l’infini

برای این همه شعارهای تو خالی

::

    Barâýé in hameh choâârhayé tou khâli
    Pour tous ces slogans vides

برای آوار خانه های پوشالی

::

    Barâýé âvâré khounehàyé pouchâli
    Pour les ruines des maisons mal construites

برای احساس آرامش

::

    Barâýé éhsassé ârâmech
    Pour le sentiment de sérénité

برای خورشید پس از شبای طولانی

::

    Barâýé khorchid pass az chabayé toulâni
    Pour la lumière qui revient après ces longues nuits

برای قرص های اعصاب و بی  خوابی

::

    Barâýé hors hayé aassâb o bi khâbi
    Pour les tranquillisants et les somnifères


برای مرد میهن آبادی

::

    Barâyé mard mihan âzâdi
    Pour l’homme, la patrie, la prospérité


برای دختری که آرزو داشت پسر بود

::

    Barâýé dokhtari ké ârézou dacht pessar boud
    Pour la fille qui voulait être un garçon

برای زن زندگی آزادی

::

    Barâyé zan zéndégui âzâdi
    Pour la femme, la vie, la liberté


برای آزادی

::

    Barâýé âzâdi
    Pour la liberté

برای آزادی

::

    Barâýé âzâdi
    Pour la liberté

برای آزادی

::

    Barâýé âzâdi
    Pour la liberté


Slam par Ladan
-------------------

.. youtube:: DdBsmp0ZANc
