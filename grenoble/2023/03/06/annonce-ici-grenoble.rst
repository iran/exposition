

.. _ici_grenoble_2023_03__06:

=================================================================================================================
Lundi 6 mars 2023 **Annonce sur ici-grenoble.org** |ici_grenoble|
=================================================================================================================

- https://www.ici-grenoble.org/evenement/vernissage-exposition-iran-femme-vie-liberte-sur-les-luttes-des-femmes-iraniennes


.. figure:: images/annonce_ici_grenoble.png
   :align: center

   https://www.ici-grenoble.org/evenement/vernissage-exposition-iran-femme-vie-liberte-sur-les-luttes-des-femmes-iraniennes



.. figure:: images/visage_peint.png
   :align: center


.. figure:: images/cut_the_oppression.png
   :align: center


Annonce de l'exposition
=============================

Vernissage de l'exposition Iran-Femme-Vie-Liberté, présentée du 7 au 31
mars 2023 à la maison de l'International Grenoble.

Vernissage en musique, avec Iran Solidarités et la Ligue pour la défense
des droits de l'homme en Iran.

L’exposition “Femme Vie Liberté” se donne comme objectif de mettre en
lumière la lutte des femmes iraniennes.

Elle décrira la répression en Iran depuis la révolution de 1979 et tentera
de retracer la révolution en cours depuis le meurtre de Mahsa Jina Amini
par le gouvernement iranien.

L’exposition comprend également une partie artistique en relation avec
“Femme Vie Liberté”, exposant des œuvres de peinture et de céramique
produites par des artistes iraniennes grenobloises.

À 18h

À la Maison de l'International
Grenoble


**Les moments forts sur Grenoble cette semaine**  |ici_grenoble|
=====================================================================

- https://www.ici-grenoble.org/agenda

.. figure:: images/lundi_6_mars_2023.png
   :align: center

   https://www.ici-grenoble.org/evenement/quand-les-femmes-se-levent-le-peuple-avance-ucl , https://www.ici-grenoble.org/evenement/soiree-anthropocene-5-ateliers-ludiques-pour-comprendre-les-crises-planetaires


.. figure:: images/mardi_7_mars_2023.png
   :align: center

   https://www.ici-grenoble.org/evenement/appel-intersyndical-a-bloquer-le-pays-contre-une-reforme-des-retraites-injuste, https://www.ici-grenoble.org/evenement/manifestation-intersyndicale-contre-la-reforme-des-retraites-acte-6, https://www.ici-grenoble.org/evenement/ag-interprofessionnelle-contre-la-reforme-des-retraites, https://www.ici-grenoble.org/evenement/soiree-jeu-histoire-femmes-et-feminisme, https://www.ici-grenoble.org/evenement/special-retraites-comment-passer-dune-lutte-contre-a-une-lutte-pour-avec-bernard-friot


.. figure:: images/mercredi_8_mars_2023.png
   :align: center

   https://www.ici-grenoble.org/evenement/appel-a-la-greve-feministe-salariale-domestique-et-reproductive, https://www.ici-grenoble.org/evenement/assemblee-generale-feministe-de-grenoble, https://www.ici-grenoble.org/evenement/manifestation-feministe-interluttes, https://www.ici-grenoble.org/evenement/soiree-dechanges-sur-les-questionnements-et-les-luttes-feministes
