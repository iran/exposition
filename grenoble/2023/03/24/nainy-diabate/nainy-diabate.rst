.. index::
   Festival détours de babel ; Naïny Diabaté
   ! Naïny Diabaté

.. _nainy_diabate_2023_03_24:

====================================================================================================================================================
Vendredi 24 mars 2023 à 18h30 **Naïny Diabaté** à la maison de l'International **(dans le cadre du festival détours de Babel, Musiques Nomades)**
====================================================================================================================================================

- :ref:`salle_2_grenoble`


.. figure:: ../../../../images/musiques_nomades.png
   :align: center

   https://musiques-nomades.fr/le-festival

- https://musiques-nomades.fr/agenda
- https://musiques-nomades.fr/agenda/nainy-diabate

.. figure:: images/nainy_diabate.png
   :align: center

Avec sa voix puissante, sa générosité artistique et son engagement féministe,
Naïny Diabaté est l’une des chanteuses griottes les plus renommées du Mali.

Également talentueuse joueuse de bolon mandingue, cette harpe-luth, basse
à trois cordes extrêmement populaire dans son pays, elle sait être à elle
seule un étendard, portant à travers rythmes ardents et textes engagés
la voix des femmes de demain !

Voix puissante, générosité artistique, engagement pour la cause des femmes
au Mali et dans le monde, création d'un ensemble de femmes griotes..

Chapeau bas, madame Naïny Diabaté !
