.. index::
   Festival détours de babel ; Rebecca Roger Cruz
   ! Rebecca Roger Cruz

.. _rebecca_roger_cruz_2023_03_24:

==========================================================================================================================================================
Vendredi 31 mars 2023 à 18h30 **Rebecca Roger Cruz** à la maison de l'International **(dans le cadre du festival détours de Babel, Musiques Nomades)**
==========================================================================================================================================================

- :ref:`salle_2_grenoble`


.. figure:: ../../../../images/musiques_nomades.png
   :align: center

   https://musiques-nomades.fr/le-festival

- https://musiques-nomades.fr/agenda
- https://musiques-nomades.fr/agenda/rebecca-roger-cruz

.. figure:: images/rebecca_roger_cruz.png
   :align: center

Les chants de l’artiste lyrique et musicologue vénézuélienne Rebecca Roger Cruz
sont un voyage dans la voix humaine, une connexion avec la nature et les
rites du vivant.

Avec cette passionnée du changement et de la mutation des voix, musiques
traditionnelles, anciennes et improvisées se rejoignent, convoquant un esprit
hybride et onirique.

Et si une telle artiste peut raviver en nous la chaleur du feu et la force
de l’ouragan, sa voix sait nous bercer comme les vagues de la mer...

Pas de réservation. Vente sur place le jour de l'événement dans la limite
des places disponibles (60 places)

Tarif de soutien au choix (de 3 à 20€)
Dans le cadre des Salons de musique
