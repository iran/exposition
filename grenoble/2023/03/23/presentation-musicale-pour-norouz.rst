.. index::
   pair: Musique; 2023-03-23

.. _musique_2023_03_23:

======================================================================================
2023-03-23 **Présentation musicale à l'occasion de Norouz** de 16 à 18h
======================================================================================

.. figure:: images/Nouvel_an_Norouz.png
   :align: center
