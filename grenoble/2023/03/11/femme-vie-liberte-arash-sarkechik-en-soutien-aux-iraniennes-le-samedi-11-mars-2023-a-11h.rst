
.. index::
   pair: Concert; 2023-03-11
   ! Arash Sarkechik

.. _concert_iran_2023_03_11:

==================================================================================================================================================================================
**Femme Vie Liberté: Arash Sarkechik en soutien aux iraniennes le samedi 11 mars 2023 à 11h  bibliothèque municipale international de Grenoble, 6 Place de Sfax 38000 Grenoble**
==================================================================================================================================================================================

- https://www.sarkechikmusic.com/
- https://www.youtube.com/@arashsarkechik2011/videos
- https://www.bm-grenoble.fr/657-bibliotheque-municipale-internationale.htm


.. figure:: images/bibliotheque.png
   :align: center


Présentation de Arash Sarkechik
===================================

.. figure:: images/arash_sarkechik.png
   :align: center

   https://musiques-nomades.fr/actions-culturelles/approche-la-creation/visite-groupe-les-secrets-du-theatre-sainte-marie-den-2


Concert à la bibliothèque municipale international de Grenoble, 6 Place de Sfax 38000 Grenoble


.. warning:: Le concert se déroule à bibliothèque municipale international
  de Grenoble, 6 Place de Sfax 38000 Grenoble

- https://www.bm-grenoble.fr/657-bibliotheque-municipale-internationale.htm


.. raw:: html

    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
    src="https://www.openstreetmap.org/export/embed.html?bbox=5.709658563137055%2C45.19440929150719%2C5.713199079036713%2C45.19602346690208&amp;layer=mapnik"
    style="border: 1px solid black"></iframe><br/><small>
    <a href="https://www.openstreetmap.org/?mlat=45.19521&mlon=5.71143#map=19/45.19521/5.71143">Afficher une carte plus grande</a></small>

Bibliothèque municipale international de Grenoble
6 Place de Sfax 38000 Grenoble


Vidéos de Arash Sarkechik
-------------------------------

- https://www.sarkechikmusic.com/toutirabien/



But du concert
==================

Dans un pays où un voile mal porté est un crime, avoir une vie normale
est un rêve.
"Femme Vie Liberté" est le cri d'un peuple qui a subi 44 ans de violence,
de discrimination, de destruction environnemenentale et patrimoniale.

Depuis le meurtre de Mahsa Jina Amini, les femmes, les minorités de genre
et d'ethnies marginalisées reprennent en force leur combat, mené depuis
plus d'un siècle. Un combat intense qui se traduit auhjord'hui en
révolution "Femme, Vie, Liberté".

C'est dans ce contexte qu'Arash Sarkechik, chanteur grenoblois d'origine
iranienne nous propose un temps musical en soutien à ce mouvement.
Multi-instrumentiste, formé à la musique classque comme au jazz, ses chansons
racontent le monde troublé d'aujourd'hui mais sans s'apitoyer sur ses
détails, elles prennent une hauteur poétique et un recul philosophique.



:download:`L'affiche au format PDF <images/Affiche_Arash_Sarkechik_2023_03_11.pdf>`


.. figure:: images/Affiche_Arash_Sarkechik_2023_03_11.png
   :align: center
   :width: 800

   **Bibliothèque municipale international de Grenoble, 6 Place de Sfax 38000 Grenoble**

Le concert va être précédé d'un discours et une présentation en slam.

Un débat clôturera le concert.


