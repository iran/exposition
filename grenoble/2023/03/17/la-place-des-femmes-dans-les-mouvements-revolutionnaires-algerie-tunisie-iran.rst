.. index::
   pair: Conférence; La place des femmes dans les mouvements revolutionnaires Algerie Tunisie Iran


.. _conference_2023_03_17:

===============================================================================================================================
Vendredi 17 mars 2023 **La place des femmes dans les mouvements revolutionnaires Algerie Tunisie Iran** par Wassyla Tamzali
===============================================================================================================================


Annonce
=========

- https://mobilizon.chapril.org/events/a1101c0a-8bd8-487d-bd55-275dfa2f302f


Description
===============

Conférence débat, la place des femmes dans les mouvements révolutionnaires:
l'Algérie, la Tunisie, l'Iran par Wassyla Tamzali

Après un rappel des luttes de femmes en Algérie, la conférence abordera
la place centrale occupée par la question des femmes dans le mouvement
de contestation appelé le Hirak débuté en Algérie en 2019.

Il n'y a pas de peuple libre sans la liberté des femmes.

L'intervenante proposera une mise en perspective des femmes en Tunisie
et le mouvement en cours en Iran.


.. figure:: images/femmes_revolutionnaires.png
   :align: center


Wassyla Tamzali : « Si la révolution iranienne réussit, ce sera un peu comme le mur de Berlin »
=====================================================================================================

- https://www.petit-bulletin.fr/grenoble/article-73651-Wassyla+Tamzali+++++Si+la+revolution+iranienne+reussit++ce+sera+un+peu+comme+le+mur+de+Berlin++.html




Conférence / Écrivaine et militante féministe, l’Algérienne Wassyla Tamzali
sera à Grenoble le 17 mars 2023 pour donner une conférence sur "La place des femmes
dans les mouvements révolutionnaires : Tunisie, Algérie, Iran".

Entretien avec une indocile de 81 ans, qui a passé sa vie à se battre
contre la domination masculine.

Les femmes ont-elles été des moteurs des mouvements populaires de contestation en Tunisie et en Algérie, étant donné que leurs conditions de vie sont globalement plus dures encore que celles des hommes ?
===============================================================================================================================================================================================================

Au départ, pour la Tunisie et l’Algérie, il y a une préoccupation sociale
certaine, qui vise les conditions de la vie en général – l’habitat, etc.

Mais ces révolutions relèvent plus d’une question de dignité que d’économie,
même si l’élément déclencheur en Tunisie est porté par un vendeur de légumes
ambulant (Mohamed Bouazizi, qui s’est donné la mort en s’immolant le
17 décembre 2010, ndlr) ; mais je pense que c’est plus parce qu’il a été
méprisé que pour sa situation économique.

Le peuple qui marche dans les rues d’Alger pendant une année (pour s’opposer
à une nouvelle candidature de Bouteflika en 2019, mouvement stoppé par
l’irruption du Covid, ndlr), c’est davantage pour la dignité que pour une
demande économique. On est loin d’une révolte du pain.

Dans cette révolte pour la dignité, en Tunisie comme en Algérie, la question
des femmes est centrale.

D’abord, parce que c’est elles qui portent le plus le fardeau des inégalités
sociales, mais aussi de l’inégalité patriarcale.
Dans ce contexte-là, la présence des femmes en tant que citoyennes est
très importante. Elles partagent les mêmes problèmes que l’ensemble de
la société, et en même temps elles sont plus défavorisées.
Et c’est sur elles que reposent aussi, en partie, les systèmes politiques.

« La domination des femmes console les hommes, d’une certaine manière,
de leur impuissance et du mépris politique dans lequel on les tient :
le seul pouvoir qu’on laisse au citoyen, c’est celui sur les femmes »

C’est-à-dire ?
================

**Les patriarcats dans les pays musulmans ont été légitimés et renforcés
par la référence à l’islam**.

Dans ces pays, c’est un **instrument de pouvoir et non de religion**.

L’islam est une idéologie politique très forte, un instrument de pouvoir
extraordinaire, autant que la domination des femmes par les hommes.
Ça console les hommes, d’une certaine manière, de leur impuissance et
du mépris politique dans lesquels on les tient.

Le citoyen, dans ces pays-là, n’a aucun pouvoir.
Le seul pouvoir qu’on lui laisse, c’est celui sur les femmes.

C’est rarement signalé, mais je crois que c’est un élément clé de la
domination des femmes dans ces pays-là.


L’islam comme idéologie politique peut-il exister sans être un outil de domination des femmes ?
===================================================================================================

Absolument pas. C’est tellement documenté que ce n’est même plus la peine
d’en parler ! D’ailleurs l’islam est devenu une morale sexuelle, dans la
mesure où il ne parle et ne s’intéresse qu’à la hiérarchie des sexes.

On ne parle que de ça ! La grande force de coercition de l’islam, c’est
qu’il porte en lui l’absence de la liberté de conscience.

S’il y avait une liberté de conscience reconnue, on pourrait discuter ;
là, on ne discute plus. On discute avec les armes, l’emprisonnement,
le voile, l’enfermement des femmes… On ne parle plus de l’islam.

Au terme des mouvements de contestation, les Tunisiennes et les Algériennes
n’ont pas obtenu l’égalité demandée.

Ma conviction est que si le Hirak (série de manifestations en 2019 et 2021, ndlr)
n’a pas réussi, c’est parce qu’on n’a pas voulu aborder la question des femmes,
qui est fondamentale.

Dans ces pays où le patriarcat est un élément de conservation des pouvoirs
en place, la révolution passe par la question des femmes.

Si j’ai choisi de mettre en perspective ces trois mouvements historiques,
c’est parce qu’ils mettent en lumière, à mon sens, le rôle prépondérant
de la question des femmes.

Et je pense **que la chance de réussite de la révolte iranienne vient du
fait que, pour la première fois dans ces pays-là, on assiste à une révolution
inaugurée par les femmes, portée par les femmes et sur le sujet des femmes**.

Cet élément me semble fondamental pour porter une révolution jusqu’à son
expression, c’est-à-dire le renversement du régime actuel.

En Algérie, un collectif de femmes, le Carré féministe, est né durant le Hirak en 2019. Que s’est-il passé ?
=================================================================================================================


Le Carré féministe a été créé par des associations pour rendre plus visible
la question des femmes à l’intérieur de la marche.

Il a mis à jour une hostilité latente contre la question des femmes dans
le Hirak.
Et tant qu’on ne l’avait pas posé d’une manière aussi claire, personne
ne s’en rendait compte. Il y avait une euphorie générale : on est tous
des frères, on est tous des sœurs, on lutte pour la liberté…

Mais dès l’instant où on pose très clairement la question de la liberté
des femmes, l’absence de consensus est révélée.
Et comme dit Jacques Rancière, une population qui marche, tant qu’elle
n’a pas accepté le dissensus, ce n’est pas un peuple.
Un peuple qui marche pour la révolution, c’est un peuple qui a résolu,
d’une manière symbolique, la question du dissensus.

Je ne suis pas politologue mais je sais très bien que les femmes, autant
que la religion, ne sont qu’une arme entre les mains du pouvoir.
Il n’empêche que ces sujets de la hiérarchie des sexes, de l’organisation
de la famille, restent l’image emblématique et visible de la grande
querelle du peuple algérien.

**J’accentue les traits pour sortir des idées toutes faites. C’est ce que
je vais essayer d’apporter à Grenoble**.

Mon expérience, et remettre les choses à la bonne place : la religion,
etc. Parce qu’en France, on a fait de l’islam le repoussoir de toute
discussion et de tout dialogue ; alors que l’islam dans les pays musulmans
n’est qu’un instrument politique.

L’islam, on lui fait dire ce qu’on veut, c’est une religion.

Vous savez, la religion ne vient pas du ciel, elle vient de la terre.
En France, l’islamophobie ne naît pas d’une guerre de religion entre les
chrétiens et les musulmans ; elle naît du racisme.


Au-delà des contestations des années 2010, comment évolue, en Tunisie et en Algérie, la situation des femmes ?
===================================================================================================================

Toutes les femmes algériennes, comme les marocaines, tunisiennes, avec
qui je préparais les conférences internationales à l’Unesco, sont nées
au féminisme dans les années 70.

En même temps qu’en Europe. Ce n’est pas l’Europe qui a fait naître le
féminisme. Le féminisme est né dans les pays arabes.

Je pense par exemple à l’Égypte, à Huda Sharawi, qui en 1925 a créé
L’Égyptienne, revue féministe... Ensuite, la bataille a été beaucoup
plus difficile car nous n’avons pas eu la période de modernité qu’a connue
l’Occident.
Les pays arabes comme l’Algérie n’ont pas pu la connaître d’abord parce
qu’ils étaient colonisés, et que la colonisation, c’était peut-être le
régime le plus rétrograde qu’on peut imaginer.

En Algérie, la France n’a été ni moderne, ni laïque, ni droits de l’homme :
elle a été tout le contraire !
Elle a obligé l’Algérie à se couler dans ce moule colonialiste, et les
réactions à ce moule ont été du même ordre

Pendant la colonisation, il y a eu chez certaines élites, en ville, une
culture urbaine qui a permis à des femmes de se moderniser.
Dans ma famille, on envoyait les filles à l’école dès les années 1930,
et même avant ; ce n’était pas du tout pour imiter les Français, c’était
pour devenir fort par rapport à cet Occident qui semblait plus fort
que nous.
Je ne peux pas prendre ma famille en exemple, mais pour vous donner une
idée, notre modernité existait contre la France – même si on envoyait
les enfants à l’école française.
Quand on achetait une ferme, on disait : on vient de libérer une petite
partie de l’Algérie !

Il y a aujourd’hui une régression des idées et de la pensée.
Mais il y a aussi une avancée du féminisme considérable chez les filles
éduquées. Ça, c’est fondamental ! Hier, j’étais avec une artiste algérienne
qui vit dans une famille extrêmement conservatrice – elle est obligée
de se voiler quand elle retourne dans son village –, mais qui est d’un
féminisme fulgurant !
Qui me donne des leçons, à moi. Nous, nous avons connu un féminisme
intellectuel, savant, parce que dans nos familles nous n’étions pas
discriminées à ce point, et nous étions portées par un mouvement de
modernité.

Mais aujourd’hui, quand nous sommes confrontés à des jeunes femmes qui
ont 20 ou 30 ans, et qui vivent dans des situations épouvantables de
pression au nom de la religion, nous sommes devant un féminisme radical,
beaucoup plus radical que le nôtre.


Pourriez-vous nous dire un mot sur le centre d’art que vous avez fondé à Alger, les Ateliers sauvages ?
============================================================================================================

Je pense qu’aujourd’hui, la réponse ne peut pas être politique,
malheureusement.

La réponse est culturelle.

Il nous faut continuer à entretenir des espaces de réflexion et de liberté,
de création.
Ce point de vue est partagé par beaucoup d’intellectuels et militants
politiques.
Le temps des révolutions a malheureusement un peu échoué (on voit bien
la situation de la Tunisie). Il faut cesser, peut-être, de faire des
attaques frontales pour réfléchir de manière plus profonde, plus
subjective.
Ma réponse à la crise dans laquelle nous vivons, c’est vraiment les
Ateliers sauvages.

Ce qu’il faut retenir, c’est que le mouvement historique de l’islam
politique est né en Iran. Et si la révolution iranienne réussit, je
pense que ce sera un peu comme le mur de Berlin.

Wassyla Tamzali conférence-débat vendredi 17 mars à 18h30 à la Maison du tourisme, entrée libre

Mini-bio
============

- 10 juillet 1941 / Naissance à Bejaïa
- 1966 / Elle devient avocate à la cour d’Alger
- 1992 / Cofondatrice du collectif Maghreb Égalité
- 1996 / Nommée directrice du programme de l’UNESCO pour la promotion
  de la condition des femmes en Méditerranée
- 2007 / Parution de Une éducation algérienne : de la révolution à la
  décennie noire, (Gallimard), prix Essai France Télévisions 2008
- 2009 / Parution de Une femme en colère ; lettre d'Alger aux Européens
  désabusés (Gallimard)
- 2010 / Parution de Burqa ? (avec Claude Ber) (Gallimard)
- 2012 / Elle dirige l’ouvrage collectif Histoires minuscules des
  révolutions arabes (Chèvre-feuille étoilée)
- 2015 / Elle fonde Les Ateliers Sauvages, un centre d’art, à Alger
- 2021 / Parution de La Tristesse est un mur entre deux jardins :
  Algérie, France, Féminisme (Odile Jacob), avec Michelle Perrot


Lieu
======

.. raw:: html

    <iframe width="600" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=5.728012919425964%2C45.189496548617214%2C5.7315534353256234%2C45.191046594089045&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19027/5.72978">Afficher une carte plus grande</a></small>


::

    Auditorium de l'office de tourisme Grenoble Alpes
    14 Rue de la République
    38000, Grenoble, France
