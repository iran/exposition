
.. _annonces_2023_03_02:

=================================================================================================================
Jeudi 2 mars 2023  Annonces officielles de la mairie: Exposition événement : **Iran • Femme • Vie • Liberté**
=================================================================================================================

- https://mastodon.lol/@emmanuelcarroz/109954921944947170


.. figure:: images/emmanuel_carroz.png
   :align: center

   https://mastodon.lol/@emmanuelcarroz/109954921944947170


Exposition événement : "Iran • Femme • Vie • Liberté "

A la @MI_Grenoble (Maison de l'International Grenoble) de la @VilledeGrenoble
du 7 au 31 mars 2023.

Vernissage le 7 mars 2023 à 18h00, en musique, avec Iran Solidarités et
la Ligue pour la défense des droits de l'homme en Iran.


.. figure:: images/annonce_officielle.png
   :align: center


Annonce
=========

Exposition Iran-Femme-Vie-Liberté du 7 au 31 mars 2023
à la maison de l'International Grenoble.

Vernissage le 7 mars 2023 à 18h00, en musique, avec Iran Solidarités et
la Ligue pour la défense des droits de l'homme en Iran.

L’exposition “Femme Vie Liberté” se donne comme objectif de mettre en
lumière la lutte des femmes iraniennes.

Elle décrira la répression en Iran depuis la révolution de 1979 et tentera
de retracer la révolution en cours depuis le meurtre de Mahsa Jina Amini
par le gouvernement iranien.

L’exposition comprend également une partie artistique en relation avec
“Femme Vie Liberté”, exposant des œuvres de peinture et de céramique
produites par des artistes iraniennes grenobloises.

- https://www.instagram.com/iranluttes/

.. figure:: images/maison_de_l_international.png
   :align: center


tags
-----

#villedegrenoble #culture #grenoble #exposition #exhibition #art #iran
#freeiran #2023 #femmevieliberté #zanzendegiazadi #jinjyanazadi #womanlifefreedom

Liens web
-----------

- https://www.instagram.com/iranluttes/
- https://femme-vie-liberte.frama.io/expositions-2023/grenoble/grenoble/grenoble.html
