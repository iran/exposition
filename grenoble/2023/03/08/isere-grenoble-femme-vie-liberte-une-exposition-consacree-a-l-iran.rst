.. index::
   pair: Dauphiné Libéré ; 8 mars 2023
   ! 8 mars 2023

.. _dl_2023_03_08:

==============================================================================================
Dauphiné Libéré: **Isère grenoble femme vie liberte une exposition consacrée à l'Iran**
==============================================================================================

- https://www.ledauphine.com/amp/culture-loisirs/2023/03/08/isere-grenoble-femme-vie-liberte-une-exposition-consacree-a-l-iran


Introduction
==============

Mardi 7 mars 2023, l’exposition artistique “Femme, vie, liberté” s’est ouverte
à la Maison de l’international de Grenoble.

L’exposition consacrée à l’Iran se clôturera le 31 mars 2023.

par Emma Larbi - 2023-03-08 à 19:09 | mis à jour 2023-03-08 à 19:11


.. figure:: images/tableau_salle_4.png
   :align: center

   Une œuvre de l’artiste iranienne Elmira Zohrehnajad.  Photo Le DL/Emma LARBI


«Espérons que le soulèvement historique en Iran apporte la démocratie
pour le peuple », a annoncé Zoreh Baharmast, présidente de la section
grenobloise de Ligue des droits de l’homme Iran, sous les hauts plafonds
de la maison de l’International de Grenoble.
