
.. _ici_grenoble_2023_03_08:

========================================================================
Mercredi 8 mars 2023 Les événements sur Ici-grenoble
========================================================================



.. figure:: ../06/images/mercredi_8_mars_2023.png
   :align: center

   https://www.ici-grenoble.org/evenement/appel-a-la-greve-feministe-salariale-domestique-et-reproductive, https://www.ici-grenoble.org/evenement/assemblee-generale-feministe-de-grenoble, https://www.ici-grenoble.org/evenement/manifestation-feministe-interluttes, https://www.ici-grenoble.org/evenement/soiree-dechanges-sur-les-questionnements-et-les-luttes-feministes

