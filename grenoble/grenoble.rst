.. index::
   ! Maison de l'International


.. figure:: ../images/logo_mahsa_amini.png
   :align: center
   :width: 300


.. _maison_international_2023_03:

================================================================================================================================
**Exposition "Iran Femme Vie Liberté" à la Maison de l'International à Grenoble du lundi 7 mars au vendredi 31 mars 2023**
================================================================================================================================

.. figure:: images/A3_expo_femme_vie_liberte.png
   :align: center
   :width: 500

   https://www.grenoble.fr/98-maison-de-l-international.htm, https://www.openstreetmap.org/#map=19/45.19225/5.72776


.. raw:: html

   <iframe width="600" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="https://www.openstreetmap.org/export/embed.html?bbox=5.725188553333283%2C45.1914775748417%2C5.729667842388153%2C45.193027566362716&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small>
   <a href="https://www.openstreetmap.org/#map=19/45.19225/5.72743">Afficher une carte plus grande</a></small>

::

    Maison de l'International
    1, rue Hector Berlioz
    Jardin de ville
    38000 Grenoble

    parvis des droits de l'Homme Jardin de Ville


.. figure:: images/entree_maison_internationale.png
   :align: center
   :width: 300

   Entrée de la maison de l'International


.. figure:: images/entree_maison_internationale_2.png
   :align: center
   :width: 300

Le Collectif Iran Solidarités et la Ligue pour la défense des droits de
l'Homme en Iran vous proposent cette exposition consacrée à L'Iran
**Femme, Vie, Liberté**, trois mots qui secouent le monde depuis plusieurs mois,
trois mots qui sont devenus l'emblème d'une révolution en marche en Iran.

Depuis la mort de Mahsa Jina Amini, ce slogan est scandé dans toutes les rues en
Iran pour réclamer les droits fondamentaux des femmes et de toute La population

Réprimé dans le sang depuis 43 ans, le peuple iranien est déterminé à reprendre
son destin en main.


.. toctree::
   :maxdepth: 5

   salles/salles
   2023/2023
   videos/videos
